package org.example;

public class Product extends Subject {

    private float price;

    public Product(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
        this.notifyObservers();
    }
}