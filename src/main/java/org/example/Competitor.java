package org.example;

public class Competitor implements Observer {

    private static final float COMPETITOR_THRESHOLD = 9.0f;

    private Product product;

    public Competitor(Product product) {
        this.product = product;
    }

    @Override
    public void update() {
        if (product.getPrice() < COMPETITOR_THRESHOLD) {
            System.out.print(
                "Competitor lowered his own price " +
                    "when watching product @ $" + product.getPrice());
        } else {
            System.out.print(
                "Competitor kept his own price " +
                    "when watching product @ $" + product.getPrice());
        }
    }
}
