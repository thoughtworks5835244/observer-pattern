package org.example;

public class Customer implements Observer {

    private static final float PURCHASE_THRESHOLD = 10.0f;

    private Product product;

    public Customer(Product product) {
        this.product = product;
    }

    @Override
    public void update() {
        if (product.getPrice() < PURCHASE_THRESHOLD) {
            System.out.print("Customer purchased the product @ $" + product.getPrice());
        } else {
            System.out.print("Customer watched the product @ $" + product.getPrice());
        }
    }
}