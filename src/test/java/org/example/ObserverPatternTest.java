package org.example;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(OrderAnnotation.class)
class ObserverPatternTest {

    static Product product;
    static Customer customer;
    static Competitor competitor;
    static ByteArrayOutputStream output;

    @BeforeAll
    static void setUpAll() {
        product = new Product(12.0f);
        customer = new Customer(product);
        competitor = new Competitor(product);
    }

    @BeforeEach
    void setUp() {
        output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
    }

    @AfterEach
    void tearDown() {
        product.detach(customer);
        product.detach(competitor);
    }

    String systemOut() {
        return output.toString();
    }

    @Test
    @Order(1)
    void should_make_correct_decision_when_price_change_given_a_customer_and_a_product() {
        product.attach(customer);
        var expected1 = "Customer watched the product @ $10.99";
        var expected2 = "Customer purchased the product @ $9.99";

        product.setPrice(10.99f);
        assertEquals(expected1, systemOut());

        product.setPrice(9.99f);
        assertEquals(expected1 + expected2, systemOut());
    }

    @Test
    @Order(2)
    void should_make_correct_decision_when_price_change_given_a_competitor_and_a_product() {
        product.attach(competitor);
        var expected1 = "Competitor kept his own price when watching product @ $9.99";
        var expected2 = "Competitor lowered his own price when watching product @ $8.99";

        product.setPrice(9.99f);
        assertEquals(expected1, systemOut());

        product.setPrice(8.99f);
        assertEquals(expected1 + expected2, systemOut());
    }

    @Test
    @Order(3)
    void competitor_should_keep_his_price_and_customer_should_buy_when_price_equals_9_given_all_parties() {
        product.attach(customer);
        product.attach(competitor);
        var expected1 = "Customer purchased the product @ $9.0";
        var expected2 = "Competitor kept his own price when watching product @ $9.0";

        product.setPrice(9.0f);

        assertEquals(expected1 + expected2, systemOut());
    }

    @Test
    @Order(4)
    void should_ignore_price_change_when_detach_customer_from_product_given_a_customer_and_a_product() {
        product.attach(customer);
        var expected = "Customer purchased the product @ $7.99";

        product.setPrice(7.99f);
        assertEquals(expected, systemOut());

        product.detach(customer);
        product.setPrice(8.99f);
        assertEquals(expected, systemOut());
    }
}
